<?php

class MessageStore {

    private array $dictionaries;
    private string $lang;
    private string $dir;

    public function __construct($dir, $lang) {
        $this->lang = $lang;
        $this->dir = $dir;

        $this->loadFile($dir, $lang);
    }

    public function switchLanguage($lang) : void {
        $this->lang = $lang;
        $this->loadFile($this->dir, $lang);
    }

    private function loadFile($dir, $lang) : void {
        if (isset($this->dictionaries[$lang])) {
            return;
        } else {
            $this->dictionaries[$lang] = [];
        }

        $lines = file(sprintf("%s/messages-%s.txt", $dir, $lang));

        foreach ($lines as $line) {
            [$key, $value] = explode('=', trim($line), 2);

            $this->dictionaries[$lang][$key] = $value;
        }
    }

    public function getAllMessages() : array {
        if (!isset($this->dictionaries[$this->lang])) {
            return [];
        }

        $result = [];
        foreach ($this->dictionaries[$this->lang] as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    public function getMessage($key, $substitutions = []) : string {
        $dict = $this->dictionaries;

        $message = $dict[$this->lang][$key] ?? '';

        return preg_replace_callback(
            '/{(\d+)}/',
            function ($matches) use ($substitutions) {
                $index = $matches[1] - 1; // counting starts from {1}.

                return $substitutions[$index] ?? '';
            },
            $message
        );
    }
}
