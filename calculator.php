<?php

require_once 'vendor/tpl.php';
require_once 'MessageStore.php';

$lang = $_GET['lang'] ?? 'et';

$store = new MessageStore('messages', $lang);

print renderTemplate('calculator.html', [], $store->getAllMessages());
