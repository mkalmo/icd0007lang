<?php

require_once 'MessageStore.php';

$store = new MessageStore('messages', 'et');

print $store->getMessage('BUTTON_CALCULATE') . PHP_EOL;

$store->switchLanguage('en');

print $store->getMessage('BUTTON_CALCULATE') . PHP_EOL;
print $store->getMessage('MESSAGE_C2F', [20, 68]) . PHP_EOL;
print count($store->getAllMessages()) . PHP_EOL;
