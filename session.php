<?php

require_once 'vendor/tpl.php';

$cmd = $_GET['cmd'] ?? 'without';

if ($cmd === 'without') {

    print renderTemplate('session.html',
        ['session' => print_r($_SESSION, true)]);

} else if ($cmd === 'start') {
    session_start();

    print renderTemplate('session.html',
        ['session' => print_r($_SESSION, true)]);

} else if ($cmd === 'login') {
    session_start();

    $_SESSION['user'] = 'Alice';

    header('Location: session.php?cmd=start');

} else if ($cmd === 'destroy') {
    session_start();
    session_destroy();

    header('Location: session.php');
}
